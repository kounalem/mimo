package com.kounalakis.mimo.data.db

import com.google.common.truth.Truth
import com.kounalakis.mimo.data.model.LessonDTO
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Test

class DataSourceImpTest {

    private val lessonsDao: LessonsDao = mock()
    private var SUT: DataSourceDb = DataSourceImp(lessonsDao)

    @Test
    fun `given lessons, when save lessons is called from data source, then return a completable object`() {
        Truth.assertThat(SUT.saveLessons(lessons = emptyList())).isInstanceOf(Completable::class.java)
    }

    @Test
    fun `when clear is called from data source, then return a completable object`() {
        Truth.assertThat(SUT.clear()).isInstanceOf(Completable::class.java)
    }

    @Test
    fun `when get all lessons is called from data source, then return a single object of the lessons`() {
        val lessons: LessonDTO = mock()
        val lessonList: List<LessonDTO> = listOf(lessons)
        val lessonSingle = Single.just(lessonList)
        whenever(lessonsDao.getAllLessons()).thenReturn(lessonSingle)

        Truth.assertThat(SUT.getAllLessons()).isEqualTo(lessonSingle)
    }

    @Test
    fun `when get lesson by id is called from data source, then return a single object of that lesson`() {
        val lesson: LessonDTO = mock()
        val expected = Single.just(lesson)
        whenever(lessonsDao.getLessonById(1)).thenReturn(expected)

        Truth.assertThat(SUT.getLessonById(1)).isEqualTo(expected)
    }

    @Test
    fun `when lessons solved is called from data source, then store this lesson`() {
        Truth.assertThat(SUT.lessonSolved(mock())).isInstanceOf(Completable::class.java)
    }

    @Test
    fun `when lessons started is called from data source, then store this lesson`() {
        Truth.assertThat(SUT.lessonSolved(mock())).isInstanceOf(Completable::class.java)
    }

}
