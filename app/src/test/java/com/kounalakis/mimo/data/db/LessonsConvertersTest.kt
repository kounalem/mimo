package com.kounalakis.mimo.data.db

import com.google.common.truth.Truth
import org.junit.Test

class LessonsConvertersTest {

    private val SUT = LessonsConverters()

    @Test
    fun `given string with three ContentDto, when toContents, then return three objects ContentDto `() {
        val given =
            "ContentDTO(id=null, color=#FF0000, text=var ),ContentDTO(id=null, color=#FFFFFF, text=number = ),ContentDTO(id=null, color=#0000FF, text=1)"

        val contents = SUT.toContents(given)

        Truth.assertThat(contents.size).isEqualTo(3)
    }

    @Test
    fun `given string with three ContentDto, when toContents, then first content equals with the first given dto`() {
        val given =
            "ContentDTO(id=null, color=#FF0000, text=var ),ContentDTO(id=null, color=#FFFFFF, text=number = ),ContentDTO(id=null, color=#0000FF, text=1)"

        Truth.assertThat(SUT.toContents(given)[0].text).isEqualTo("var ")
    }

    @Test
    fun `given string with three ContentDto, when toContents, then first content equals with the second given dto`() {
        val given =
            "ContentDTO(id=null, color=#FF0000, text=var ),ContentDTO(id=null, color=#FFFFFF, text=number = ),ContentDTO(id=null, color=#0000FF, text=1)"

        Truth.assertThat(SUT.toContents(given)[1].text).isEqualTo("number = ")
    }

    @Test
    fun `given string with three ContentDto, when toContents, then first content equals with the third given dto`() {
        val given =
            "ContentDTO(id=null, color=#FF0000, text=var ),ContentDTO(id=null, color=#FFFFFF, text=number = ),ContentDTO(id=null, color=#0000FF, text=1)"

        Truth.assertThat(SUT.toContents(given)[2].text).isEqualTo("1")
    }

    @Test
    fun `given string with one ContentDto, when toContents, then return one objects ContentDto `() {
        val given = "ContentDTO(id=null, color=#FF0000, text=var ,"

        val contents = SUT.toContents(given)

        Truth.assertThat(contents.size).isEqualTo(1)

        Truth.assertThat(contents[0].text).isEqualTo("var ")
        Truth.assertThat(contents[1].text).isEqualTo("number = ")
        Truth.assertThat(contents[2].text).isEqualTo("1")
    }

}
