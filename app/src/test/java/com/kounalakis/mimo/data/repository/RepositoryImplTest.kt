package com.kounalakis.mimo.data.repository

import com.kounalakis.mimo.data.db.DataSourceDb
import com.kounalakis.mimo.data.mapper.LessonDtoMapper
import com.kounalakis.mimo.data.mapper.ListMapper
import com.kounalakis.mimo.data.mapper.Mapper
import com.kounalakis.mimo.data.model.LessonDTO
import com.kounalakis.mimo.data.model.LessonsDTO
import com.kounalakis.mimo.data.network.ApiService
import com.kounalakis.mimo.domain.model.Lesson
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Test

class RepositoryImplTest {

    private val apiService: ApiService = mock()
    private val dataSourceDb: DataSourceDb = mock()
    private val lessonsMapper: ListMapper<LessonDTO?, Lesson> = mock()
    private val lessonMapper: Mapper<LessonDTO?, Lesson> = mock()
    private val lessonDtoMapper: LessonDtoMapper = mock()
    private val SUT =
        RepositoryImpl(apiService, dataSourceDb, lessonsMapper, lessonMapper, lessonDtoMapper)

    @Test
    fun `given a error, when get lessons from network and db, return the dedicated error`() {
        whenever(apiService.getLessons()).thenReturn(Single.error(Throwable("this is my message from Remote")))
        whenever(dataSourceDb.getAllLessons()).thenReturn(Single.error(Throwable("this is my message from DB")))

        SUT.getLessons().test().assertErrorMessage("this is my message from DB")
    }

    @Test
    fun `given a error, when get lessons from network, return the database data`() {
        val lessonDto: LessonDTO = mock()
        val lessonsDto = listOf(lessonDto)
        val lessonsSingle: Single<List<LessonDTO>> = Single.just(lessonsDto)
        val lesson: Lesson = mock()
        val expected = listOf(lesson)
        whenever(apiService.getLessons()).thenReturn(Single.error(Throwable("this is my message from Remote")))
        whenever(dataSourceDb.getAllLessons()).thenReturn(lessonsSingle)
        whenever(lessonsMapper.map(lessonsDto)).thenReturn(expected)

        SUT.getLessons().test().assertResult(expected)
    }

    @Test
    fun `given a lesson list with data, when get lessons from network, return the dedicated db response with the latest data`() {
        val lessonDto: LessonDTO = mock()
        val lessonsDto = listOf(lessonDto)
        val lessonsSingle: Single<List<LessonDTO>> = Single.just(lessonsDto)
        val lesson: Lesson = mock()
        val expected = listOf(lesson)

        val lessonsDt: LessonsDTO = mock() {
            whenever(it.lessonListContainer).thenReturn(mock())
        }

        whenever(apiService.getLessons()).thenReturn(Single.just(lessonsDt))
        whenever(dataSourceDb.saveLessons(any())).thenReturn(Completable.complete())
        whenever(dataSourceDb.getAllLessons()).thenReturn(lessonsSingle)
        whenever(lessonsMapper.map(lessonsDto)).thenReturn(expected)

        SUT.getLessons().test().assertResult(expected)
    }

    @Test
    fun `given a lesson list without data, when get lessons from network, return the dedicated db response`() {
        val lessonDto: LessonDTO = mock()
        val lessonsDto = listOf(lessonDto)
        val lessonsSingle: Single<List<LessonDTO>> = Single.just(lessonsDto)
        val lesson: Lesson = mock()
        val expected = listOf(lesson)

        whenever(apiService.getLessons()).thenReturn(Single.just(mock()))
        whenever(dataSourceDb.saveLessons(any())).thenReturn(Completable.complete())
        whenever(dataSourceDb.getAllLessons()).thenReturn(lessonsSingle)
        whenever(lessonsMapper.map(lessonsDto)).thenReturn(expected)

        SUT.getLessons().test().assertResult(expected)
    }

    @Test
    fun `given an id, when get lessons by id, return the dedicated db response`() {
        val lessonDto: LessonDTO = mock()
        val lesson: Lesson = mock()
        whenever(dataSourceDb.getLessonById(1)).thenReturn(Single.just(lessonDto))
        whenever(lessonMapper.map(lessonDto)).thenReturn(lesson)

        SUT.getLessonById(1).test().assertResult(lesson)
    }

    @Test
    fun `when solved lesson is called, return complete`() {
        val lessonDto: LessonDTO = mock()
        val lesson: Lesson = mock()

        whenever(lessonDtoMapper.map(lesson)).thenReturn(lessonDto)
        whenever(dataSourceDb.lessonSolved(lessonDto)).thenReturn(Completable.complete())

        SUT.lessonSolved(lesson).test().assertComplete()
    }

    @Test
    fun `when started lesson is called, return complete`() {
        val lessonDto: LessonDTO = mock()
        val lesson: Lesson = mock()

        whenever(lessonDtoMapper.map(lesson)).thenReturn(lessonDto)
        whenever(dataSourceDb.lessonStarted(lessonDto)).thenReturn(Completable.complete())

        SUT.lessonStarted(lesson).test().assertComplete()
    }

}
