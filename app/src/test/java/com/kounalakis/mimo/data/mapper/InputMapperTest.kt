package com.kounalakis.mimo.data.mapper

import com.google.common.truth.Truth
import com.kounalakis.mimo.data.model.InputDTO
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test

class InputMapperTest {

    private val SUT = InputMapper()

    @Test
    fun `given endIndex, when map, then map response to the model`() {
        val given: InputDTO = mock {
            whenever(it.endIndex).thenReturn(1)
        }

        val response = SUT.map(given)

        Truth.assertThat(response.endIndex).isEqualTo(1)
    }

    @Test
    fun `given no endIndex, when map, then map default response to the model`() {
        val response = SUT.map(mock())

        Truth.assertThat(response.endIndex).isEqualTo(0)
    }

    @Test
    fun `given startIndex, when map, then map response to the model`() {
        val given: InputDTO = mock {
            whenever(it.startIndex).thenReturn(1)
        }

        val response = SUT.map(given)

        Truth.assertThat(response.startIndex).isEqualTo(1)
    }

    @Test
    fun `given no startIndex, when map, then map default response to the model`() {
        val response = SUT.map(mock())

        Truth.assertThat(response.startIndex).isEqualTo(0)
    }
}
