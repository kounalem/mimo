package com.kounalakis.mimo.data.network

import com.google.common.truth.Truth
import com.kounalakis.mimo.data.model.LessonsDTO
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Test

class ApiServiceImplTest {

    private val api: Api = mock()
    private val SUT = ApiServiceImpl(api)

    @Test
    fun `given a lesson list, when get lessons from network, return the dedicated network response`() {
        val expected: Single<LessonsDTO?> = Single.just(mock())
        whenever(api.getLessons()).thenReturn(expected)

        val result = SUT.getLessons()

        Truth.assertThat(result).isEqualTo(expected)
    }

    @Test
    fun `given a error, when get lessons from network, return the dedicated network error`() {
        whenever(api.getLessons()).thenReturn(Single.error(Throwable("this is my message")))

        SUT.getLessons().test().assertErrorMessage("this is my message")
    }

}
