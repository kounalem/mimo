package com.kounalakis.mimo.data.mapper

import com.kounalakis.mimo.data.model.ContentDTO
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import com.google.common.truth.Truth.assertThat

class ContentMapperTest {

    private val SUT = ContentMapper()

    @Test
    fun `given color, when map, then map response to the model`() {
        val given: ContentDTO = mock {
            whenever(it.color).thenReturn("color")
        }

        val response = SUT.map(given)

        assertThat(response.color).isEqualTo("color")
    }

    @Test
    fun `given no color, when map, then map default response to the model`() {
        val response = SUT.map(mock())

        assertThat(response.color).isEqualTo("")
    }

    @Test
    fun `given text, when map, then map response to the model`() {
        val given: ContentDTO = mock {
            whenever(it.text).thenReturn("text")
        }

        val response = SUT.map(given)

        assertThat(response.text).isEqualTo("text")
    }

    @Test
    fun `given no text, when map, then map default response to the model`() {
        val response = SUT.map(mock())

        assertThat(response.text).isEqualTo("")
    }

}
