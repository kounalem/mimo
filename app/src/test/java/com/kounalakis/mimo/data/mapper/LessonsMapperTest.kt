package com.kounalakis.mimo.data.mapper

import com.google.common.truth.Truth
import com.kounalakis.mimo.data.model.ContentDTO
import com.kounalakis.mimo.data.model.InputDTO
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import com.kounalakis.mimo.data.model.LessonDTO
import com.kounalakis.mimo.data.model.LessonsDTO
import com.kounalakis.mimo.domain.model.Lesson
import com.nhaarman.mockitokotlin2.any

class LessonsMapperTest {

    private val lessonsMapper: ListMapper<LessonDTO?, Lesson> = mock()
    private val SUT = LessonsMapper(lessonsMapper)

    @Test
    fun `given no lessons, when map, then map default response to the model`() {
        val response = SUT.map(mock())

        Truth.assertThat(response.lessons).isEqualTo(emptyList<Lesson>())
    }

    @Test
    fun `given lessons, when map, then map response to the model`() {
        val expected: List<Lesson> = mock()
        whenever(lessonsMapper.map(any())).thenReturn(expected)
        val given: LessonsDTO = mock {
            whenever(it.lessonListContainer).thenReturn(mock())
        }

        val response = SUT.map(given)

        Truth.assertThat(response.lessons).isEqualTo(expected)
    }

}
