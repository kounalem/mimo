package com.kounalakis.mimo.data.mapper

import com.google.common.truth.Truth
import com.kounalakis.mimo.data.model.ContentDTO
import com.kounalakis.mimo.data.model.InputDTO
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import com.kounalakis.mimo.data.model.LessonDTO
import com.kounalakis.mimo.domain.model.Content
import com.kounalakis.mimo.domain.model.Input
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.notNull

class LessonMapperTest {

    private val inputMapper = mock<InputMapper>()
    private val contentListMapper: ListMapper<ContentDTO?, Content> = mock()
    private val SUT = LessonMapper(inputMapper, contentListMapper)

    @Test
    fun `given id, when map, then map response to the model`() {
        val given: LessonDTO = mock {
            whenever(it.id).thenReturn(1)
        }

        val response = SUT.map(given)

        Truth.assertThat(response.id).isEqualTo(1)
    }

    @Test
    fun `given no id, when map, then map default response to the model`() {
        val response = SUT.map(mock())

        Truth.assertThat(response.id).isEqualTo(0)
    }

    @Test
    fun `given input, when map, then map response to the model`() {

        val inputDto = mock<InputDTO>()
        val expected = mock<Input>()
        whenever(inputMapper.map(inputDto)).thenReturn(expected)
        val given: LessonDTO = mock {
            whenever(it.input).thenReturn(inputDto)
        }

        val response = SUT.map(given)

        Truth.assertThat(response.input).isEqualTo(expected)
    }

    @Test
    fun `given no input, when map, then map default response to the model`() {
        val response = SUT.map(mock())

        Truth.assertThat(response.input).isEqualTo(Input.DEFAULT)
    }

    @Test
    fun `given no contents, when map, then map default response to the model`() {
        val response = SUT.map(mock())

        Truth.assertThat(response.contents).isEqualTo(emptyList<Content>())
    }

    @Test
    fun `given contents, when map, then map response to the model`() {
        val expected = emptyList<Content>()
        whenever(contentListMapper.map(any())).thenReturn(expected)
        val contentListContainer = arrayListOf<ContentDTO>()
        val given: LessonDTO = mock {
            whenever(it.contents).thenReturn(contentListContainer)
        }

        val response = SUT.map(given)

        Truth.assertThat(response.contents).isEqualTo(expected)
    }

    @Test
    fun `given started, when map, then map response to the model`() {
        val given: LessonDTO = mock {
            whenever(it.started).thenReturn(1L)
        }

        val response = SUT.map(given)

        Truth.assertThat(response.started).isEqualTo(1L)
    }

    @Test
    fun `given no started, when map, then map default response to the model`() {
        val response = SUT.map(mock())

        Truth.assertThat(response.started).isEqualTo(0)
    }

    @Test
    fun `given finished, when map, then map response to the model`() {
        val given: LessonDTO = mock {
            whenever(it.finished).thenReturn(1L)
        }

        val response = SUT.map(given)

        Truth.assertThat(response.finished).isEqualTo(1L)
    }

    @Test
    fun `given no finished, when map, then map default response to the model`() {
        val response = SUT.map(mock())

        Truth.assertThat(response.finished).isEqualTo(0)
    }

    @Test
    fun `given solved, when map, then map response to the model`() {
        val given: LessonDTO = mock {
            whenever(it.solved).thenReturn(true)
        }

        val response = SUT.map(given)

        Truth.assertThat(response.solved).isEqualTo(true)
    }

    @Test
    fun `given no solved, when map, then map default response to the model`() {
        val response = SUT.map(mock())

        Truth.assertThat(response.solved).isEqualTo(false)
    }

    @Test
    fun `given input and content with start and end index, when check for existing editable content, then map response to the model`() {
        val inputDto = mock<InputDTO> {
            whenever(it.startIndex).thenReturn(0)
            whenever(it.endIndex).thenReturn(4)
        }
        val expected = mock<Input>()
        whenever(inputMapper.map(inputDto)).thenReturn(expected)
        whenever(contentListMapper.map(any())).thenReturn(mock())
        val content: ContentDTO = mock {
            whenever(it.text).thenReturn("this is an example")
        }
        val given: LessonDTO = mock {
            whenever(it.input).thenReturn(inputDto)
            whenever(it.contents).thenReturn(arrayListOf(content))
        }

        val response = SUT.map(given)

        Truth.assertThat(response.editContent).isEqualTo("this")
    }

    @Test
    fun `given input and content without start and end index, when check for existing editable content, then map response to the model`() {
        val inputDto = mock<InputDTO> {
            whenever(it.startIndex).thenReturn(0)
            whenever(it.endIndex).thenReturn(0)
        }
        val expected = mock<Input>()
        whenever(inputMapper.map(inputDto)).thenReturn(expected)
        whenever(contentListMapper.map(any())).thenReturn(mock())
        val content: ContentDTO = mock {
            whenever(it.text).thenReturn("this is an example")
        }
        val given: LessonDTO = mock {
            whenever(it.input).thenReturn(inputDto)
            whenever(it.contents).thenReturn(arrayListOf(content))
        }

        val response = SUT.map(given)

        Truth.assertThat(response.editContent).isNull()
    }

    @Test
    fun `given input and no with start and end index, when check for existing editable content, then map response to the model`() {
        val inputDto = mock<InputDTO> {
            whenever(it.startIndex).thenReturn(0)
            whenever(it.endIndex).thenReturn(2)
        }
        val expected = mock<Input>()
        whenever(inputMapper.map(inputDto)).thenReturn(expected)
        whenever(contentListMapper.map(any())).thenReturn(mock())
        val given: LessonDTO = mock {
            whenever(it.input).thenReturn(inputDto)
            whenever(it.contents).thenReturn(arrayListOf())
        }

        val response = SUT.map(given)

        Truth.assertThat(response.editContent).isNull()
    }

    @Test
    fun `given input and content with end index as string size, when check for existing editable content, then map response to the model`() {
        val inputDto = mock<InputDTO> {
            whenever(it.startIndex).thenReturn(0)
            whenever(it.endIndex).thenReturn(4)
        }
        val expected = mock<Input>()
        whenever(inputMapper.map(inputDto)).thenReturn(expected)
        whenever(contentListMapper.map(any())).thenReturn(mock())
        val content: ContentDTO = mock {
            whenever(it.text).thenReturn("this")
        }
        val given: LessonDTO = mock {
            whenever(it.input).thenReturn(inputDto)
            whenever(it.contents).thenReturn(arrayListOf(content))
        }

        val response = SUT.map(given)

        Truth.assertThat(response.editContent).isEqualTo("this")
    }

    @Test
    fun `given input and content with end index bigger than start index, when check for existing editable content, then map response to the model`() {
        val inputDto = mock<InputDTO> {
            whenever(it.startIndex).thenReturn(2)
            whenever(it.endIndex).thenReturn(1)
        }
        val expected = mock<Input>()
        whenever(inputMapper.map(inputDto)).thenReturn(expected)
        whenever(contentListMapper.map(any())).thenReturn(mock())
        val content: ContentDTO = mock {
            whenever(it.text).thenReturn("this")
        }
        val given: LessonDTO = mock {
            whenever(it.input).thenReturn(inputDto)
            whenever(it.contents).thenReturn(arrayListOf(content))
        }

        val response = SUT.map(given)

        Truth.assertThat(response.editContent).isNull()
    }
}
