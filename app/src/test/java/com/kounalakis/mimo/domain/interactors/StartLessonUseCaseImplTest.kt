package com.kounalakis.mimo.domain.interactors

import com.kounalakis.mimo.domain.repository.Repository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert.assertTrue
import org.junit.Test
import java.lang.Exception
import java.util.concurrent.TimeUnit

class StartLessonUseCaseImplTest {

    private val repository: Repository = mock()
    private val computationThread = TestScheduler()
    private val uiThread = TestScheduler()
    private val SUT = StartLessonUseCaseImpl(repository, computationThread, uiThread)

    @Test
    fun `given a lesson list, when invoke, return the successful response`() {
        whenever(repository.lessonStarted(any())).thenReturn(Completable.complete())
        var expected = false

        SUT.invoke(
            mock(),
            success = { expected = true },
            fail = { throw Exception("Will fail") }
        )
        computationThread.advanceTimeBy(1L, TimeUnit.SECONDS)
        uiThread.advanceTimeBy(1L, TimeUnit.SECONDS)

        assertTrue(expected)
    }

    @Test
    fun `given a error, when invoke, return the error`() {

        whenever(repository.lessonStarted(any())).thenReturn(Completable.error(Throwable("this is my message")))
        var expected = false

        SUT.invoke(
            mock(),
            success = { Exception("Will fail") },
            fail = { expected = true }
        )
        computationThread.advanceTimeBy(1L, TimeUnit.SECONDS)
        uiThread.advanceTimeBy(1L, TimeUnit.SECONDS)

        assertTrue(expected)
    }

}
