package com.kounalakis.mimo.domain.interactors

import com.kounalakis.mimo.domain.model.Lesson
import com.kounalakis.mimo.domain.repository.Repository
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.concurrent.TimeUnit

class GetLessonByIdUseCaseImplTest {

    private val repository: Repository = mock()
    private val computationThread = TestScheduler()
    private val uiThread = TestScheduler()
    private val SUT = GetLessonByIdUseCaseImpl(repository, computationThread, uiThread)

    @Test
    fun `given a lesson list, when invoke, return the successful response`() {
        whenever(repository.getLessonById(1)).thenReturn(Single.just(mock()))
        var expected = false

        SUT.invoke(
            1,
            success = { expected = true },
            fail = { throw Exception("Will fail") }
        )
        computationThread.advanceTimeBy(1L, TimeUnit.SECONDS)
        uiThread.advanceTimeBy(1L, TimeUnit.SECONDS)

        assertTrue(expected)
    }

    @Test
    fun `given a error, when invoke, return the error`() {
        whenever(repository.getLessonById(1)).thenReturn(Single.error(Throwable("this is my message")))
        var expected = false

        SUT.invoke(
            1,
            success = {  throw Exception("Will fail")},
            fail = {expected = true}
        )
        computationThread.advanceTimeBy(1L, TimeUnit.SECONDS)
        uiThread.advanceTimeBy(1L, TimeUnit.SECONDS)

        assertTrue(expected)
    }

}
