package com.kounalakis.mimo.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.kounalakis.mimo.RxSynchronizeSchedulersRule
import com.kounalakis.mimo.domain.interactors.GetLessonByIdUseCase
import com.kounalakis.mimo.domain.interactors.SolveLessonUseCase
import com.kounalakis.mimo.domain.interactors.StartLessonUseCase
import com.kounalakis.mimo.domain.model.Content
import com.kounalakis.mimo.domain.model.Lesson
import com.kounalakis.mimo.presentation.lesson.LessonViewModel
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class LessonViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    var rxJavaRule: TestRule = RxSynchronizeSchedulersRule()

    private val errorObserver: Observer<String> = mock()
    private val solveObserver: Observer<Unit> = mock()
    private val editContentObserver: Observer<String> = mock()
    private val textContentObserver: Observer<Content> = mock()
    private val buttonEnabledObserver: Observer<Boolean> = mock()

    private val getLessonUseCase: GetLessonByIdUseCase = mock()
    private val startUseCase: StartLessonUseCase = mock()
    private val solveUseCase: SolveLessonUseCase = mock()

    private val SUT = LessonViewModel(
        getLessonUseCase,
        startUseCase,
        solveUseCase
    )

    @Before
    fun setUp() {
        SUT.error.observeForever(errorObserver)
        SUT.solved.observeForever(solveObserver)
        SUT.editContent.observeForever(editContentObserver)
        SUT.buttonEnabled.observeForever(buttonEnabledObserver)
        SUT.textContent.observeForever(textContentObserver)
    }

    @Test
    fun `given a lesson response, when invoke lesson by id, then save that lesson`() {
        whenever(startUseCase.invoke(any(), any(), any())).thenReturn(mock())
        whenever(getLessonUseCase.invoke(1, any(), any())).thenReturn(mock())

        SUT.getLessonById(1)

        verify(startUseCase.invoke(any(), any(), any()))
    }

    @Test
    fun `given an error response, when invoke lesson by id, then the error observer is triggered`() {
        whenever(startUseCase.invoke(any(), any(), any())).thenThrow(Throwable())
        whenever(getLessonUseCase.invoke(1, any(), any())).thenReturn(mock())

        SUT.getLessonById(1)

        verify(errorObserver.onChanged(any()))
    }

    @Test
    fun `given an error response, when invoke lesson by id, then the button observer is triggered`() {
        whenever(startUseCase.invoke(any(), any(), any())).thenThrow(Throwable())
        whenever(getLessonUseCase.invoke(1, any(), any())).thenReturn(mock())

        SUT.getLessonById(1)

        verify(buttonEnabledObserver.onChanged(any()))
    }

    @Test
    fun `given an save error response, when invoke lesson by id, , then the error observer is triggered`() {
        whenever(startUseCase.invoke(any(), any(), any())).thenReturn(any())
        whenever(getLessonUseCase.invoke(1, any(), any())).thenThrow(Throwable())

        SUT.getLessonById(1)

        verify(errorObserver.onChanged(any()))
    }

    @Test
    fun `given an save error response, when invoke lesson by id, , then the button observer is triggered`() {
        whenever(startUseCase.invoke(any(), any(), any())).thenReturn(any())
        whenever(getLessonUseCase.invoke(1, any(), any())).thenThrow(Throwable())

        SUT.getLessonById(1)

        verify(buttonEnabledObserver.onChanged(false))
    }

    @Test
    fun `given an solved lesson, when invoke solve lesson, then the solved observer is triggered`() {
        val lesson: Lesson = mock()
        whenever(solveUseCase.invoke(lesson, any(), any())).thenReturn(mock())

        SUT.solveLesson()

        verify(solveObserver.onChanged(any()))
    }

    @Test
    fun `given an solved lesson, when invoke solve lesson, then the error observer is triggered`() {
        val lesson: Lesson = mock()
        whenever(solveUseCase.invoke(lesson, any(), any())).thenThrow(Throwable())

        SUT.solveLesson()

        verify(errorObserver.onChanged(any()))
    }

    @Test
    fun `given an solved lesson, when invoke solve lesson, then the button observer is triggered`() {
        val lesson: Lesson = mock()
        whenever(solveUseCase.invoke(lesson, any(), any())).thenThrow(Throwable())

        SUT.solveLesson()

        verify(buttonEnabledObserver.onChanged(any()))
    }

}
