package com.kounalakis.mimo.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.kounalakis.mimo.RxSynchronizeSchedulersRule
import com.kounalakis.mimo.domain.interactors.GetLessonsUseCase
import com.kounalakis.mimo.domain.model.Lesson
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class MainViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    var rxJavaRule: TestRule = RxSynchronizeSchedulersRule()


    private val errorObserver: Observer<Unit> = mock()
    private val lessonsObserver: Observer<List<Lesson>> = mock()
    private val getLessonsUseCase: GetLessonsUseCase = mock()
    private val SUT = MainViewModel(getLessonsUseCase)

    @Before
    fun setUp() {
        SUT.error.observeForever(errorObserver)
        SUT.lessons.observeForever(lessonsObserver)
    }

    @Test
    fun `given a list response, when invoke data, then the list observer is triggered`() {
        whenever(
            getLessonsUseCase.invoke(any(), any())
        ).thenReturn(mock())

        SUT.getLessons()

        verify(lessonsObserver.onChanged(any()))
    }

    @Test
    fun `given an error response, when invoke data, then the error observer is triggered`() {
        whenever(getLessonsUseCase.invoke(any(), any())).thenThrow(Throwable())

        SUT.getLessons()

        verify(errorObserver.onChanged(any()))
    }
}
