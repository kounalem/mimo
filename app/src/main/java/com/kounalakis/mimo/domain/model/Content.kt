package com.kounalakis.mimo.domain.model

data class Content(val id: Int, val color: String, val text: String)
