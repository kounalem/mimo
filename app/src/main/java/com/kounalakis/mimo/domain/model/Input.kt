package com.kounalakis.mimo.domain.model

data class Input(val id: Int, val startIndex: Int, val endIndex: Int) {
    companion object {
        val DEFAULT = Input(0, 0, 0)
    }
}
