package com.kounalakis.mimo.domain.interactors

import com.kounalakis.mimo.domain.model.Lesson
import io.reactivex.disposables.Disposable

interface GetLessonByIdUseCase {
    operator fun invoke(
        id: Int,
        success: (lessons: Lesson) -> Unit,
        fail: (throwable: Throwable) -> Unit
    ): Disposable
}
