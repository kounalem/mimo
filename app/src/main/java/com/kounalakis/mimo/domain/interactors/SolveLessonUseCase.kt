package com.kounalakis.mimo.domain.interactors

import com.kounalakis.mimo.domain.model.Lesson
import io.reactivex.disposables.Disposable

interface SolveLessonUseCase {
    operator fun invoke(
        lesson: Lesson,
        success: () -> Unit,
        fail: (throwable: Throwable) -> Unit
    ): Disposable
}
