package com.kounalakis.mimo.domain.model

data class Lessons(val lessons: List<Lesson>)
