package com.kounalakis.mimo.domain.interactors

import com.kounalakis.mimo.domain.model.Lesson
import com.kounalakis.mimo.domain.repository.Repository
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SolveLessonUseCaseImpl(
    private val repository: Repository,
    private val computation: Scheduler = Schedulers.computation(),
    private val uIThread: Scheduler = AndroidSchedulers.mainThread()
) : SolveLessonUseCase {

    override fun invoke(lesson: Lesson, success: () -> Unit, fail: (throwable: Throwable) -> Unit): Disposable {
        return repository.lessonSolved(lesson)
            .subscribeOn(computation)
            .observeOn(uIThread)
            .subscribe({
                success()
            }, {
                fail(it)
            })
    }

}
