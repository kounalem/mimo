package com.kounalakis.mimo.domain.model

data class Lesson(
    val id: Int,
    val contents: List<Content>,
    val input: Input,
    val solved: Boolean,
    val started: Long,
    val finished: Long,
    val editContent: String?
)
