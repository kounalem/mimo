package com.kounalakis.mimo.domain.repository

import com.kounalakis.mimo.domain.model.Lesson
import io.reactivex.Completable
import io.reactivex.Single

interface Repository {

    fun getLessons(): Single<List<Lesson>>
    fun getLessonById(id: Int): Single<Lesson>
    fun lessonSolved(lesson: Lesson): Completable
    fun lessonStarted(lesson: Lesson): Completable

}
