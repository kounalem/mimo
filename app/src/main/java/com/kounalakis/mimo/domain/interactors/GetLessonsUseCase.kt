package com.kounalakis.mimo.domain.interactors

import com.kounalakis.mimo.domain.model.Lesson
import io.reactivex.disposables.Disposable

interface GetLessonsUseCase {
    operator fun invoke(
        success: (lessons: List<Lesson>) -> Unit,
        fail: (throwable: Throwable) -> Unit
    ): Disposable
}
