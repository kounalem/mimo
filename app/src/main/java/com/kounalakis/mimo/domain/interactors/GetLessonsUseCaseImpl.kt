package com.kounalakis.mimo.domain.interactors

import com.kounalakis.mimo.domain.model.Lesson
import com.kounalakis.mimo.domain.repository.Repository
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class GetLessonsUseCaseImpl(
    private val repository: Repository,
    private val computation: Scheduler = Schedulers.computation(),
    private val UIThread: Scheduler = AndroidSchedulers.mainThread()
) :
    GetLessonsUseCase {

    override operator fun invoke(
        success: (lessons: List<Lesson>) -> Unit,
        fail: (throwable: Throwable) -> Unit
    ): Disposable {
        return repository.getLessons()
            .subscribeOn(computation)
            .observeOn(UIThread)
            .subscribe({
                success(it)
            }, {
                fail(it)
            })
    }

}
