package com.kounalakis.mimo.presentation.routers

interface LessonRouter {
    fun navigate(id: Int)
}
