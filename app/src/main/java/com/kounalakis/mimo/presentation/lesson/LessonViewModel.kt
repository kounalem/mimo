package com.kounalakis.mimo.presentation.lesson

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kounalakis.mimo.domain.interactors.*
import com.kounalakis.mimo.domain.model.Content
import com.kounalakis.mimo.domain.model.Lesson
import com.kounalakis.mimo.presentation.base.BaseViewModel
import timber.log.Timber

class LessonViewModel(
    private val getLessonById: GetLessonByIdUseCase,
    private val startLesson: StartLessonUseCase,
    private val solveLesson: SolveLessonUseCase
) : BaseViewModel() {

    private val _textContent = MutableLiveData<Content>()
    val textContent: LiveData<Content>
        get() = _textContent

    private val _editContent = MutableLiveData<String>()
    val editContent: LiveData<String>
        get() = _editContent

    private var _userInput = ""
    val userInput: CharSequence
        get() = _userInput

    private val _buttonEnabled = MutableLiveData<Boolean>()
    val buttonEnabled: LiveData<Boolean>
        get() = _buttonEnabled

    private val _error = MutableLiveData<String>()
    val error: LiveData<String>
        get() = _error

    private val _solved = MutableLiveData<Unit>()
    val solved: LiveData<Unit>
        get() = _solved

    private var lesson: Lesson? = null
    private var editable: String? = null

    fun getLessonById(id: Int) {
        add(
            getLessonById(
                id,
                {
                    lesson = it
                    triggerObservables(it)
                    startLesson(it)
                }, {
                    disableButtonAndShowError(it)
                })
        )
    }

    private fun triggerObservables(lesson: Lesson) {
        if (lesson.editContent != null) {
            lesson.contents.forEach {
                if (it.text.contains(lesson.editContent, ignoreCase = true)) {
                    editable = lesson.editContent
                    _buttonEnabled.value = false
                    _editContent.value = it.color
                    _textContent.value = Content(0, it.color, it.text.replace(lesson.editContent, ""))
                } else {
                    _textContent.value = it
                }
            }
        } else {
            lesson.contents.forEach {
                _textContent.value = it
            }
            _buttonEnabled.value = true
        }
    }

    private fun startLesson(lesson: Lesson) {
        if (lesson.started > 0L) {
            return
        }

        add(
            startLesson(
                lesson,
                {
                    Timber.d("saved start lesson timestamp successfully ")
                }, {
                    disableButtonAndShowError(it)
                })
        )
    }

    fun solveLesson() {
        _buttonEnabled.value = false
        lesson?.let {
            add(
                solveLesson(it,
                    {
                        _buttonEnabled.value = true
                        _solved.value = Unit
                    }, { error ->
                        disableButtonAndShowError(error)
                    })
            )
        }
    }

    private fun disableButtonAndShowError(throwable: Throwable) {
        _buttonEnabled.value = false
        _error.value = throwable.message
    }

    fun checkUserInput(input: String) {
        _userInput = input
        _buttonEnabled.value = input.equals(editable ?: "", ignoreCase = true)
    }
}
