package com.kounalakis.mimo.presentation.lesson

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.kounalakis.mimo.presentation.FragmentCallback
import com.kounalakis.mimo.presentation.view.LessonView
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class LessonFragment : Fragment() {

    companion object {
        @JvmStatic
        fun createFragment(lessonId: Int): LessonFragment {
            return LessonFragment().apply {
                this.lessonId = lessonId
            }
        }
    }

    private lateinit var fragmentCallback: FragmentCallback
    private var lessonId: Int = -1
    private val lessonView: LessonView by inject {
        parametersOf(lifecycle, viewLifecycleOwner, {
            fragmentCallback.goToNextLesson()
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = FrameLayout(requireActivity())
        root.addView(lessonView)
        return root
    }

    override fun onResume() {
        super.onResume()
        lessonView.getLessonById(lessonId)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fragmentCallback = context as FragmentCallback
    }

    override fun onPause() {
        lessonView.hideKeyBoard()
        super.onPause()
    }

}
