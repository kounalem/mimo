package com.kounalakis.mimo.presentation.utils

import android.widget.Button
import androidx.databinding.BindingAdapter

@BindingAdapter("bind:enable")
fun setEnable(button: Button, enable: Boolean?) {
    enable?.let {
        button.isEnabled = enable
    }
}

