package com.kounalakis.mimo.presentation.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : LifecycleObserver, ViewModel() {

    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    fun add(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun cleanUp() {
        compositeDisposable.clear()
    }
}
