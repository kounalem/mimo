package com.kounalakis.mimo.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kounalakis.mimo.domain.interactors.GetLessonsUseCase
import com.kounalakis.mimo.domain.model.Lesson
import com.kounalakis.mimo.presentation.base.BaseViewModel

class MainViewModel(
    private val getLessons: GetLessonsUseCase
) : BaseViewModel() {

    private val _lessons = MutableLiveData<List<Lesson>>()
    val lessons: LiveData<List<Lesson>>
        get() = _lessons

    private val _error = MutableLiveData<Unit>()
    val error: LiveData<Unit>
        get() = _error

    fun getLessons() {
        add(
            getLessons(
                {
                    _lessons.value = it
                }, {
                    _error.value = Unit
                })
        )
    }

}
