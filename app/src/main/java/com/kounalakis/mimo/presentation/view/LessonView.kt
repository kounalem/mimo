package com.kounalakis.mimo.presentation.view

import android.content.Context
import android.graphics.Color
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.kounalakis.mimo.R
import com.kounalakis.mimo.databinding.LessonFragmentBinding
import com.kounalakis.mimo.presentation.lesson.LessonViewModel
import com.kounalakis.mimo.presentation.utils.Keyboard.Companion.hideKeyboard
import com.kounalakis.mimo.presentation.utils.Keyboard.Companion.showKeyboard
import com.kounalakis.mimo.presentation.utils.MimoTextWatcher

class LessonView(
    private val lifecycle: Lifecycle,
    private val lifecycleOwner: LifecycleOwner,
    private val lessonSolved: () -> Unit,
    private val viewModel: LessonViewModel,
    context: Context
) : FrameLayout(context) {

    private val binding: LessonFragmentBinding = DataBindingUtil.inflate(
        LayoutInflater.from(context),
        R.layout.lesson_fragment,
        this,
        true
    )
    val root: View
        get() = binding.root

    init {
        binding.viewModel = viewModel
        binding.lifecycleOwner = lifecycleOwner
        lifecycle.addObserver(viewModel)
        setUpObservers()
        listeners()
    }

    private fun listeners() {
        binding.btnProceed.setOnClickListener {
            viewModel.solveLesson()
        }
    }

    private fun setUpObservers() {

        viewModel.error.observe(lifecycleOwner, Observer {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        })

        viewModel.solved.observe(lifecycleOwner, Observer {
            lessonSolved()
        })

        viewModel.textContent.observe(lifecycleOwner, Observer {
            addTextView(it.text, it.color)
        })

        viewModel.editContent.observe(lifecycleOwner, Observer {
            addEditText(it)
        })
    }

    fun getLessonById(id: Int) {
        binding.llQuestionContainer.removeAllViews()
        viewModel.getLessonById(id)
    }

    private fun addTextView(text: String, color: String) {
        val textView = TextView(context)
        textView.text = text
        textView.setTextColor(Color.parseColor(color))
        textView.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        binding.llQuestionContainer.addView(textView)
        binding.llQuestionContainer.invalidate()
    }

    private fun addEditText(color: String) {
        val editText = EditText(context)
        editText.setTextColor(Color.parseColor(color))

        if (viewModel.userInput.isNotEmpty()) {
            editText.setText(viewModel.userInput)
            viewModel.checkUserInput(viewModel.userInput.toString())
        }

        editText.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        binding.llQuestionContainer.addView(editText)
        editChangeListener(editText)

        editText.isFocusableInTouchMode = true
        editText.isFocusable = true
        editText.requestFocus()
        showKeyboard(context, editText)

        binding.llQuestionContainer.invalidate()
    }

    private fun editChangeListener(editText: EditText) {
        editText.addTextChangedListener(
            object : MimoTextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    super.afterTextChanged(s)
                    s?.let { viewModel.checkUserInput(s.toString()) }
                }
            }
        )
    }

    fun hideKeyBoard() {
        hideKeyboard(context, windowToken)
    }

}
