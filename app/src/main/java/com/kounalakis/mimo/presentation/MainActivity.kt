package com.kounalakis.mimo.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kounalakis.mimo.R
import com.kounalakis.mimo.presentation.routers.CompletedRouter
import com.kounalakis.mimo.presentation.routers.LessonRouter
import com.kounalakis.mimo.presentation.view.MainView
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class MainActivity : AppCompatActivity(), FragmentCallback {

    private val lessonRouter: LessonRouter by inject { parametersOf(this) }
    private val completedRouter: CompletedRouter by inject { parametersOf(this) }
    private val mainView: MainView by inject { parametersOf(lessonRouter, completedRouter, this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainView.getLessons()
    }

    override fun onStop() {
        mainView.stop()
        super.onStop()
    }

    override fun goToNextLesson() {
        mainView.goToNextLesson()
    }

}
