package com.kounalakis.mimo.presentation

interface FragmentCallback {
    fun goToNextLesson()
}
