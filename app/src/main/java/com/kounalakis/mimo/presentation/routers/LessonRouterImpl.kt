package com.kounalakis.mimo.presentation.routers

import androidx.appcompat.app.AppCompatActivity
import com.kounalakis.mimo.R
import com.kounalakis.mimo.presentation.lesson.LessonFragment

class LessonRouterImpl(private val appCompatActivity: AppCompatActivity) : LessonRouter {

    override fun navigate(id: Int) {
        appCompatActivity.supportFragmentManager.beginTransaction()
            .replace(
                R.id.container,
                LessonFragment.createFragment(id), LessonFragment::class.java.simpleName
            )
            .commitNow()
    }

}
