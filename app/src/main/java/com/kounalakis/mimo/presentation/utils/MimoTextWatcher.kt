package com.kounalakis.mimo.presentation.utils

import android.text.Editable
import android.text.TextWatcher

interface MimoTextWatcher : TextWatcher {

    override fun afterTextChanged(s: Editable?) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

}
