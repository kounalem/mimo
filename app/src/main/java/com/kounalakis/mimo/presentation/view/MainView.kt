package com.kounalakis.mimo.presentation.view

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import com.kounalakis.mimo.R
import com.kounalakis.mimo.presentation.MainViewModel
import com.kounalakis.mimo.presentation.routers.LessonRouter

import androidx.lifecycle.Observer
import com.kounalakis.mimo.presentation.routers.CompletedRouter

class MainView(
    private val lessonRouter: LessonRouter,
    private val completedRouter: CompletedRouter,
    private val viewModel: MainViewModel,
    private val lifecycleOwner: LifecycleOwner,
    private val context: Context
) {

    init {
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.lessons.observe(lifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                val currentLesson = it.firstOrNull { item -> !item.solved }
                currentLesson?.let {
                    lessonRouter.navigate(currentLesson.id)
                } ?: completedRouter.navigate()
            }
        })
        viewModel.error.observe(lifecycleOwner, Observer {
            Toast.makeText(context, context.getString(R.string.could_not_retrieve_lessons), Toast.LENGTH_SHORT).show()
        })
    }

    fun getLessons() {
        viewModel.getLessons()
    }

    fun stop() {
        viewModel.cleanUp()
    }

    fun goToNextLesson() {
        viewModel.getLessons()
    }

}
