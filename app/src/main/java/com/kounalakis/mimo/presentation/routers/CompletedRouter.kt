package com.kounalakis.mimo.presentation.routers

interface CompletedRouter {
    fun navigate()
}
