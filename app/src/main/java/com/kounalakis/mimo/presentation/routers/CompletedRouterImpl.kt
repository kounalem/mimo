package com.kounalakis.mimo.presentation.routers

import androidx.appcompat.app.AppCompatActivity
import com.kounalakis.mimo.R
import com.kounalakis.mimo.presentation.completed.CompletedFragment

class CompletedRouterImpl(private val appCompatActivity: AppCompatActivity) : CompletedRouter {

    override fun navigate() {
        appCompatActivity.supportFragmentManager.beginTransaction()
            .replace(
                R.id.container,
                CompletedFragment(), CompletedFragment::class.java.simpleName
            )
            .commitNow()
    }

}
