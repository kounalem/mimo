package com.kounalakis.mimo.di

import com.kounalakis.mimo.domain.interactors.*
import org.koin.dsl.module

val useCaseModule = module {
    single<GetLessonsUseCase> {
        GetLessonsUseCaseImpl(get())
    }

    single<SolveLessonUseCase> {
        SolveLessonUseCaseImpl(get())
    }

    single<StartLessonUseCase> {
        StartLessonUseCaseImpl(get())
    }

    single<GetLessonByIdUseCase> {
        GetLessonByIdUseCaseImpl(get())
    }

}

val domainModule = listOf(useCaseModule)
