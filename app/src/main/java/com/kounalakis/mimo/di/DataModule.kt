package com.kounalakis.mimo.di

import androidx.room.Room
import com.kounalakis.mimo.data.db.AppDatabase
import com.kounalakis.mimo.data.db.DataSourceDb
import com.kounalakis.mimo.data.db.DataSourceImp
import com.kounalakis.mimo.data.db.LessonsDao
import com.kounalakis.mimo.data.mapper.*
import com.kounalakis.mimo.data.model.ContentDTO
import com.kounalakis.mimo.data.model.InputDTO
import com.kounalakis.mimo.data.model.LessonDTO
import com.kounalakis.mimo.data.network.Api
import com.kounalakis.mimo.data.network.ApiService
import com.kounalakis.mimo.data.network.ApiServiceImpl
import com.kounalakis.mimo.data.network.RequestInterceptor
import com.kounalakis.mimo.data.repository.RepositoryImpl
import com.kounalakis.mimo.domain.model.Content
import com.kounalakis.mimo.domain.model.Input
import com.kounalakis.mimo.domain.model.Lesson
import com.kounalakis.mimo.domain.repository.Repository
import okhttp3.Cache
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.StringQualifier
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val mappingModule = module {

    single<Mapper<InputDTO?, Input>>(StringQualifier("InputMapper")) {
        InputMapper()
    }

    single<Mapper<ContentDTO?, Content>>(StringQualifier("ContentMapper")) {
        ContentMapper()
    }
    single<Mapper<LessonDTO?, Lesson>>(StringQualifier("LessonMapper")) {
        LessonMapper(
            contentListDataMapping = ListMapperImpl(get(StringQualifier("ContentMapper"))),
            inputDataMapping = get(StringQualifier("InputMapper"))
        )
    }
    single<ListMapper<LessonDTO?, Lesson>>(StringQualifier("LessonListMapper")) {
        ListMapperImpl(get(StringQualifier("LessonMapper")))
    }

}

val repoModule = module {

    single<ApiService> {
        ApiServiceImpl(get())
    }

    single<Repository> {
        RepositoryImpl(
            apiService = get(),
            dataSourceDb = get(),
            lessonsMapper = get(StringQualifier("LessonListMapper")),
            lessonMapper = get(StringQualifier("LessonMapper"))
        )
    }

}

val networkModule = module {

    single {
        val cacheSize = 10 * 1024 * 1024
        Cache(androidContext().cacheDir, cacheSize.toLong())
    }

    fun provideClientBuild(cache: Cache) = OkHttpClient.Builder().addInterceptor(RequestInterceptor())
        .cache(cache)
        .connectTimeout(15, TimeUnit.SECONDS)
        .build()

    single {
        provideClientBuild(get())
    }

    fun provideRetrofitBuild(client: OkHttpClient) = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl("https://mimochallenge.azurewebsites.net/api/")
        .client(client)
        .build()

    single {
        provideRetrofitBuild(get()).create(Api::class.java)
    }

}

val dataBaseModule = module {

    single<AppDatabase> {
        Room.databaseBuilder(get(), AppDatabase::class.java, "mimo_db.db").build()
    }

    fun provideLessonsDao(localDatabase: AppDatabase): LessonsDao = localDatabase.lessonsDao

    single<LessonsDao> {
        provideLessonsDao(get())
    }

    fun provideDataSource(lessonsDao: LessonsDao): DataSourceDb = DataSourceImp(lessonsDao)

    single<DataSourceDb> {
        provideDataSource(get())
    }

}

val dataModule = listOf(mappingModule, dataBaseModule, networkModule, repoModule)
