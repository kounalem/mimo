package com.kounalakis.mimo.di

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.kounalakis.mimo.presentation.lesson.LessonViewModel
import com.kounalakis.mimo.presentation.MainViewModel
import com.kounalakis.mimo.presentation.routers.CompletedRouter
import com.kounalakis.mimo.presentation.routers.CompletedRouterImpl
import com.kounalakis.mimo.presentation.routers.LessonRouter
import com.kounalakis.mimo.presentation.routers.LessonRouterImpl
import com.kounalakis.mimo.presentation.view.LessonView
import com.kounalakis.mimo.presentation.view.MainView
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel<LessonViewModel> {
        LessonViewModel(
            getLessonById = get(),
            startLesson = get(),
            solveLesson = get()
        )
    }

    viewModel<MainViewModel> {
        MainViewModel(getLessons = get())
    }

}

val routerModule = module {
    factory<LessonRouter> { (activity: AppCompatActivity) -> LessonRouterImpl(activity) }
    factory<CompletedRouter> { (activity: AppCompatActivity) -> CompletedRouterImpl(activity) }
}

val viewModule = module {

    factory<MainView> { (lessonRouter: LessonRouter, completedRouter: CompletedRouter, lifecycleOwner: LifecycleOwner) ->
        MainView(
            completedRouter = completedRouter,
            lessonRouter = lessonRouter,
            viewModel = get(),
            lifecycleOwner = lifecycleOwner,
            context = androidContext()
        )
    }

    factory<LessonView> { (lifecycle: Lifecycle, lifecycleOwner: LifecycleOwner, lessonSolved: () -> Unit) ->
        LessonView(
            lifecycle = lifecycle,
            viewModel = get(),
            lifecycleOwner = lifecycleOwner,
            context = androidContext(),
            lessonSolved = lessonSolved
        )
    }

}

val presentationModule = listOf(viewModelModule, routerModule, viewModule)
