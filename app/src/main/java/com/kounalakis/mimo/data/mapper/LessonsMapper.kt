package com.kounalakis.mimo.data.mapper

import com.kounalakis.mimo.data.model.LessonDTO
import com.kounalakis.mimo.data.model.LessonsDTO
import com.kounalakis.mimo.domain.model.Lesson
import com.kounalakis.mimo.domain.model.Lessons

class LessonsMapper(
    private val lessonListDataMapping: ListMapper<LessonDTO?, Lesson>
) : Mapper<LessonsDTO?, Lessons> {

    override fun map(input: LessonsDTO?): Lessons {
        return Lessons(
            lessons = mapLessons(input?.lessonListContainer)
        )
    }

    private fun mapLessons(input: ArrayList<LessonDTO>?): List<Lesson> {
        return if (input == null || input.isNullOrEmpty()) {
            emptyList()
        } else {
            lessonListDataMapping.map(input)
        }
    }
}
