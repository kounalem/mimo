package com.kounalakis.mimo.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "input")
data class InputDTO(

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    val id: Int?,

    @ColumnInfo(name = "startIndex")
    @SerializedName("startIndex") val startIndex: Int?,

    @ColumnInfo(name = "endIndex")
    @SerializedName("endIndex") val endIndex: Int?

)
