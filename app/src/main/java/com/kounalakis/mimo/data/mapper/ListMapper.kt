package com.kounalakis.mimo.data.mapper

interface ListMapper<Input, Output> : Mapper<List<Input>?, List<Output>>
