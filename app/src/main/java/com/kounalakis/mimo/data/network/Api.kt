package com.kounalakis.mimo.data.network

import com.kounalakis.mimo.data.model.LessonsDTO
import io.reactivex.Single
import retrofit2.http.GET

interface Api {

    @GET("lessons")
    fun getLessons(): Single<LessonsDTO?>

}
