package com.kounalakis.mimo.data.db

import androidx.room.TypeConverter
import com.kounalakis.mimo.data.model.ContentDTO
import com.kounalakis.mimo.data.model.InputDTO
import com.kounalakis.mimo.data.model.LessonDTO

class LessonsConverters {

    @TypeConverter
    fun fromLessons(fromLessons: ArrayList<LessonDTO>?): String? {
        var string = ""

        if (fromLessons == null) {
            return string
        }

        fromLessons.forEach {
            string += "$it,"
        }
        return string.substring(0, string.length - 1)
    }

    @TypeConverter
    fun fromContent(contentFrom: ContentDTO?): String? {
        return contentFrom?.id.toString() + "," + contentFrom?.color + "," + contentFrom?.text
    }

    @TypeConverter
    fun fromContents(fromContents: ArrayList<ContentDTO>?): String {

        var string = ""

        if (fromContents == null) {
            return string
        }

        for (item in fromContents) {
            string += "$item,"
        }

        return string.substring(0, string.length - 1)
    }

    @TypeConverter
    fun fromInput(inputFrom: InputDTO?): String? {
        return inputFrom?.id.toString() + "," + inputFrom?.startIndex + "," + inputFrom?.endIndex
    }


    @TypeConverter
    fun toLessons(value: String?): ArrayList<LessonDTO> {
        if (value == null || value.isEmpty()) {
            return arrayListOf()
        }

        val list: List<String> = value.split(",")
        val contentList = ArrayList<LessonDTO>()
        for (item in list) {
            if (item.isNotEmpty()) {
                val pieces = item.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                val id = if (pieces[0].contains("null")) {
                    0
                } else {
                    pieces[0].toInt()
                }

                val contents = toContents(pieces[1])
                val input = toInput(pieces[2])
                contentList.add(LessonDTO(id, contents, input))
            }
        }
        return contentList
    }

    @TypeConverter
    fun toContent(contentTo: String?): ContentDTO {
        if (contentTo == null) {
            return ContentDTO(0, "", "")
        }

        var tideUpValue = contentTo.replace("ContentDTO(", "")
        tideUpValue = tideUpValue.replace("),", "")
        tideUpValue = tideUpValue.replace(" color=", "")
        tideUpValue = tideUpValue.replace(" text=", "")
        tideUpValue = tideUpValue.replace(")", "")

        val pieces = tideUpValue.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val id = if (pieces[0].contains("null")) {
            0
        } else {
            pieces[0].toInt()
        }

        val text = pieces[2]
        val color = pieces[1]

        return ContentDTO(id, color, text)
    }

    @TypeConverter
    fun toInput(inputTo: String?): InputDTO {
        if (inputTo == null) {
            return InputDTO(0, 0, 0)
        }

        val pieces = inputTo.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        if (pieces[0].contains("null") && pieces[1].contains("null") && pieces[2].contains("null")) {
            return InputDTO(id = 0, startIndex = 0, endIndex = 0)
        }
        val id = if (pieces[0].contains("null")) {
            0
        } else {
            pieces[0].toInt()
        }

        val end = pieces[2].toInt()
        val start = pieces[1].toInt()
        return InputDTO(id, start, end)
    }

    @TypeConverter
    fun toContents(value: String?): ArrayList<ContentDTO> {
        if (value == null || value.isEmpty()) {
            return arrayListOf()
        }
        var tideUpValue = value.replace("ContentDTO(", "")
        tideUpValue = tideUpValue.replace("),", "")
        tideUpValue = tideUpValue.replace(" color=", "")
        tideUpValue = tideUpValue.replace(" text=", "")
        tideUpValue = tideUpValue.replace(")", "")

        val list: List<String> = tideUpValue.split("id=")
        val contentList = ArrayList<ContentDTO>()
        for (item in list) {
            if (item.isNotEmpty()) {

                val pieces = item.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                val id = if (pieces[0].contains("null")) {
                    0
                } else {
                    pieces[0].toInt()
                }

                val text = pieces[2]
                val color = pieces[1]

                contentList.add(ContentDTO(id, color, text))
            }
        }
        return contentList
    }

}
