package com.kounalakis.mimo.data.db

import com.kounalakis.mimo.data.model.LessonDTO
import io.reactivex.Completable
import io.reactivex.Single

class DataSourceImp(var lessonsDao: LessonsDao) : DataSourceDb {

    override fun getAllLessons(): Single<List<LessonDTO>> = lessonsDao.getAllLessons()

    override fun saveLessons(lessons: List<LessonDTO>) = Completable.fromAction { lessonsDao.saveLessons(lessons) }

    override fun clear(): Completable = Completable.fromAction { lessonsDao.clear() }

    override fun lessonSolved(lesson: LessonDTO): Completable = Completable.fromAction {
        val solvedLesson = lesson.copy(solved = true, finished = System.currentTimeMillis() / 1000)
        lessonsDao.saveLesson(solvedLesson)
    }

    override fun lessonStarted(lesson: LessonDTO): Completable = Completable.fromAction {
        val solvedLesson = lesson.copy(started = System.currentTimeMillis() / 1000)
        lessonsDao.saveLesson(solvedLesson)
    }

    override fun getLessonById(id: Int): Single<LessonDTO> = lessonsDao.getLessonById(id)

}
