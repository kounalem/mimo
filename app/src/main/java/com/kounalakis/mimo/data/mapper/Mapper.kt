package com.kounalakis.mimo.data.mapper

interface Mapper<Input, Output> {
    fun map(input: Input): Output
}
