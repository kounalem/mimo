package com.kounalakis.mimo.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "content")
data class ContentDTO(

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    val id: Int?,

    @ColumnInfo(name = "color")
    @SerializedName("color")
    val color: String?,

    @ColumnInfo(name = "text")
    @SerializedName("text")
    val text: String?

)
