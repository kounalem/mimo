package com.kounalakis.mimo.data.model

import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity(tableName = "lesson")
data class LessonDTO(

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id") val id: Int?,

    @ColumnInfo(name = "contents")
    @SerializedName("content") val contents: ArrayList<ContentDTO>?,

    @ColumnInfo(name = "input")
    @SerializedName("input") val input: InputDTO?,

    @ColumnInfo(name = "solved")
    val solved: Boolean? = false,

    @ColumnInfo(name = "started")
    val started: Long? = 0L,

    @ColumnInfo(name = "finished")
    val finished: Long? = 0L

)
