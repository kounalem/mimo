package com.kounalakis.mimo.data.mapper

import com.kounalakis.mimo.data.model.ContentDTO
import com.kounalakis.mimo.data.model.InputDTO
import com.kounalakis.mimo.data.model.LessonDTO
import com.kounalakis.mimo.domain.model.Content
import com.kounalakis.mimo.domain.model.Input
import com.kounalakis.mimo.domain.model.Lesson

class LessonDtoMapper(
) : Mapper<Lesson, LessonDTO> {

    override fun map(input: Lesson): LessonDTO {
        return LessonDTO(
            contents = listContainerToDto(input.contents),
            input = inputToDto(input.input),
            id = input.id,
            solved = input.solved,
            started = input.started,
            finished = input.finished
        )
    }

    private fun inputToDto(input: Input): InputDTO {
        return InputDTO(input.id, input.startIndex, input.endIndex)
    }

    private fun listContainerToDto(contents: List<Content>): ArrayList<ContentDTO> {
        val contentDtoList = arrayListOf<ContentDTO>()
        contents.forEach {
            contentDtoList.add(contentToDto(it))
        }

        return contentDtoList
    }

    private fun contentToDto(content: Content): ContentDTO {
        return ContentDTO(content.id, content.color, content.text)
    }

}
