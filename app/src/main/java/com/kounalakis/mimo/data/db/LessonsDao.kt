package com.kounalakis.mimo.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kounalakis.mimo.data.model.LessonDTO
import io.reactivex.Single

@Dao
interface LessonsDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun saveLessons(lessons: List<LessonDTO>)

    @Query("SELECT * FROM lesson")
    fun getAllLessons(): Single<List<LessonDTO>>

    @Query("DELETE FROM lessons")
    fun clear()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveLesson(lesson: LessonDTO)

    @Query("SELECT * FROM lesson WHERE id =:id")
    fun getLessonById(id: Int? = 0): Single<LessonDTO>

}
