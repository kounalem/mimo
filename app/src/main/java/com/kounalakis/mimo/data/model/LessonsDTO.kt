package com.kounalakis.mimo.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "lessons")
data class LessonsDTO(

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    val id: Int?,

    @ColumnInfo(name = "lessons")
    @SerializedName("lessons")
    val lessonListContainer: ArrayList<LessonDTO>?

)
