package com.kounalakis.mimo.data.network

import com.kounalakis.mimo.data.model.LessonsDTO
import io.reactivex.Single

class ApiServiceImpl(private val api: Api) : ApiService {

    override fun getLessons(): Single<LessonsDTO?> {
        return api.getLessons()
    }

}
