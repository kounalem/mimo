package com.kounalakis.mimo.data.mapper

import com.kounalakis.mimo.data.model.ContentDTO
import com.kounalakis.mimo.domain.model.Content

class ContentMapper : Mapper<ContentDTO?, Content> {

    override fun map(input: ContentDTO?): Content {
        return Content(
            id = input?.id ?: 0,
            color = input?.color ?: "",
            text = input?.text ?: ""
        )
    }

}
