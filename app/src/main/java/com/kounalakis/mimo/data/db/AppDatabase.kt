package com.kounalakis.mimo.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kounalakis.mimo.data.model.ContentDTO
import com.kounalakis.mimo.data.model.InputDTO
import com.kounalakis.mimo.data.model.LessonDTO
import com.kounalakis.mimo.data.model.LessonsDTO

@Database(
    entities = [ContentDTO::class, InputDTO::class, LessonDTO::class, LessonsDTO::class
    ], version = 1, exportSchema = false
)
@TypeConverters(value = [LessonsConverters::class])
abstract class AppDatabase : RoomDatabase() {
    abstract val lessonsDao: LessonsDao
}
