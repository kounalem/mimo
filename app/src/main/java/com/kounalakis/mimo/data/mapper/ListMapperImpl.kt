package com.kounalakis.mimo.data.mapper

class ListMapperImpl<Input, Output>(private val mapper: Mapper<Input, Output>) : ListMapper<Input, Output> {

    override fun map(input: List<Input>?): List<Output> {
        return input?.let {
            it.map { item -> mapper.map(item) }
        } ?: emptyList()
    }
}
