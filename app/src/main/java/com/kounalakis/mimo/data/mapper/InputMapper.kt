package com.kounalakis.mimo.data.mapper

import com.kounalakis.mimo.data.model.InputDTO
import com.kounalakis.mimo.domain.model.Input

class InputMapper : Mapper<InputDTO?, Input> {
    override fun map(input: InputDTO?): Input {
        return Input(
            id = input?.id ?: 0,
            endIndex = input?.endIndex ?: 0,
            startIndex = input?.startIndex ?: 0
        )
    }
}
