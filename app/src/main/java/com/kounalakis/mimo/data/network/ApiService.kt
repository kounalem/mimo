package com.kounalakis.mimo.data.network

import com.kounalakis.mimo.data.model.LessonsDTO
import io.reactivex.Single

interface ApiService {
    fun getLessons(): Single<LessonsDTO?>
}
