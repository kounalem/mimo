package com.kounalakis.mimo.data.mapper

import com.kounalakis.mimo.data.model.ContentDTO
import com.kounalakis.mimo.data.model.InputDTO
import com.kounalakis.mimo.data.model.LessonDTO
import com.kounalakis.mimo.domain.model.Content
import com.kounalakis.mimo.domain.model.Input
import com.kounalakis.mimo.domain.model.Lesson

class LessonMapper(
    private val inputDataMapping: Mapper<InputDTO?, Input>,
    private val contentListDataMapping: ListMapper<ContentDTO?, Content>
) : Mapper<LessonDTO?, Lesson> {
    override fun map(input: LessonDTO?): Lesson {
        return Lesson(
            id = input?.id ?: 0,
            contents = mapContent(input?.contents),
            input = mapInput(input?.input),
            started = input?.started ?: -1L,
            solved = input?.solved ?: false,
            finished = input?.finished ?: -1L,
            editContent = checkForExistingEditableContent(
                input?.input?.startIndex ?: 0,
                input?.input?.endIndex ?: 0,
                input?.contents ?: emptyList()
            )
        )
    }

    private fun checkForExistingEditableContent(startIndex: Int, endIndex: Int, contents: List<ContentDTO>): String? {
        return if (endIndex > 0 && startIndex >= 0) {
            var allContent = ""
            for (item in contents) {
                allContent += item.text
            }

            if (startIndex > allContent.length || endIndex > allContent.length || startIndex < 0 || endIndex < startIndex) {
                return null
            }
            return allContent.substring(startIndex, endIndex)
        } else {
            null
        }
    }

    private fun mapInput(input: InputDTO?): Input {
        return if (input == null) {
            Input.DEFAULT
        } else {
            inputDataMapping.map(input)
        }
    }

    private fun mapContent(input: ArrayList<ContentDTO>?): List<Content> {
        return if (input == null || input.isNullOrEmpty()) {
            emptyList()
        } else {
            contentListDataMapping.map(input)
        }
    }
}
