package com.kounalakis.mimo.data.repository

import com.kounalakis.mimo.data.db.DataSourceDb
import com.kounalakis.mimo.data.mapper.LessonDtoMapper
import com.kounalakis.mimo.data.mapper.ListMapper
import com.kounalakis.mimo.data.mapper.Mapper
import com.kounalakis.mimo.data.model.LessonDTO
import com.kounalakis.mimo.data.network.ApiService
import com.kounalakis.mimo.domain.model.Lesson
import com.kounalakis.mimo.domain.repository.Repository
import io.reactivex.Single

class RepositoryImpl(
    private val apiService: ApiService,
    private val dataSourceDb: DataSourceDb,
    private val lessonsMapper: ListMapper<LessonDTO?, Lesson>,
    private val lessonMapper: Mapper<LessonDTO?, Lesson>,
    private val lessonDtoMapper: LessonDtoMapper = LessonDtoMapper()
) : Repository {

    override fun getLessons(): Single<List<Lesson>> {
        return apiService.getLessons()
            .flatMap { lessonContainer ->
                lessonContainer.lessonListContainer?.let { lessonListContainer ->
                    dataSourceDb.saveLessons(lessonListContainer)
                        .andThen(dataSourceDb.getAllLessons())
                        .map { localLessons ->
                            lessonsMapper.map(localLessons)
                        }
                } ?: throw Error("empty list")
            }
            .onErrorResumeNext {
                dataSourceDb.getAllLessons()
                    .map {
                        return@map lessonsMapper.map(it)
                    }
            }
    }

    override fun getLessonById(id: Int): Single<Lesson> {
        return dataSourceDb.getLessonById(id).map {
            lessonMapper.map(it)
        }
    }

    override fun lessonSolved(lesson: Lesson) = dataSourceDb.lessonSolved(lessonDtoMapper.map(lesson))

    override fun lessonStarted(lesson: Lesson) = dataSourceDb.lessonStarted(lessonDtoMapper.map(lesson))

}


