package com.kounalakis.mimo.data.db

import com.kounalakis.mimo.data.model.LessonDTO
import io.reactivex.Completable
import io.reactivex.Single

interface DataSourceDb {

    fun getAllLessons(): Single<List<LessonDTO>>
    fun saveLessons(lessons: List<LessonDTO>): Completable
    fun clear(): Completable
    fun lessonSolved(lesson: LessonDTO): Completable
    fun getLessonById(id: Int): Single<LessonDTO>
    fun lessonStarted(lesson: LessonDTO): Completable
}
