package com.kounalakis.mimo

import android.app.Application
import com.kounalakis.mimo.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class MimoApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        prepareKoin()
        prepareTimber()
    }

    private fun prepareKoin() {
        startKoin {
            androidContext(this@MimoApplication)
            modules(dataModule + domainModule + presentationModule)
        }
    }

    private fun prepareTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}
