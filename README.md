# Mimo
This app, displays the lessons returned from the Mimo's server and allows to the user view the next lesson, if one has solved
 the previous one. If the lesson contains an interaction, then display the interaction. Also display a "Next" button that, 
 when pressed, checks that the lesson has been solved and continues to the next lesson. When a lesson has been solved,
  store this event in the mobile database. If the user solved the last lesson, display a simple screen that says "Done".

### Requirements:
Android Studio 3.6.3 and API 29 build tools and libraries installed

### Design approach:
The Uncle's Bob clean architecture is being used here, with keeping in mind of course that the 5 solid principles are also kept.
Despite being simple in features, I wanted the project to showcase the use of the MVVM design pattern to improve the overall
 architecture of the app, allowing the behaviour of the app to be unit testable. The app is available both online and offline.

### Libraries used:
App: Java RX 2, Retroft, Koin, GSON, Timber, Room. For testing: Truth and mockito.

